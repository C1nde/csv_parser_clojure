## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

You will need to install deps

    lein deps

## Running

To start a web server for the application, run:

    lein run

To buld front-end javascript(already built):

    lein cljsbuild auto
