(ns myapp.core
  (:require [reagent.core :as reagent :refer [atom]]
            [secretary.core :as secretary]
            [reagent-forms.core :refer [bind-fields]]
            [ajax.core :refer [POST]])
  (:require-macros [secretary.core :refer [defroute]]))

(def data (atom {:url "https://raw.githubusercontent.com/incanter/incanter/master/data/hair_eye_color.csv"}))

(defn table-header [cols]
  [:tr (map (fn [col] [:th (name col)]) cols)]
)

(defn table-row [row]
  [:tr (map (fn [k] [:td (row k)]) (keys row))]
)

(defn table [rows]
  [:table.table.table-bordered
    [:thead (table-header (keys (first rows)))]
    [:tbody (map #(table-row %) rows)]])

(defn save []
  (POST "/get_table"
        {:params @data
         :handler (fn [response]
            (reagent/render-component [#(table response)] (.getElementById js/document "table"))
          )}))

(defn row [input]
  [:div.form-group input])

(def form-template
  [:div
    (row [:input.form-control {:field :text :id :url}])
    [:button {:class "btn btn-default btn-block":on-click save} "Save"]])

(defn form []
  [bind-fields form-template data])

(defn init! []
  (reagent/render-component [form] (.getElementById js/document "form"))
)
